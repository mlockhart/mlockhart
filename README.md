# 🥷 "I fight for the users!"

I'm MikeL (@mlockhart), a [GitLab Support Engineer](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html) in APAC East. I like Open Source software, Open Architecture computers, Open Data formats, and Open Crown hats.

Here are some quick links:

- 👔 You can view my [GitLab Engineering README](https://about.gitlab.com/handbook/engineering/readmes/mike-lockhart/) for more information about working with me.
- 🎯 See what I'm thinking and hacking on in my [logs board](https://gitlab.com/mlockhart/lab/-/boards/1894499?label_name[]=log).
- 💙 My non-Work GitLab profile, [@milohax](https://gitlab.com/milohax), has projects not related to my job (but I do use [some of them](https://gitlab.com/milohax-net/radix) in my work).
- 🔐 Need to share secrets? Here are my [GitLab-registered Public SSH keys](https://gitlab.com/mlockhart.keys) and my [Keybase Public PGP key](https://keybase.io/sinewalker/pgp_keys.asc?fingerprint=840093b416fea6048473a2f63cca2e6ebcbe8795).

###### 🚫 Private and confidential links:

- ♻️ [Weekly recurring Work Log](https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=Weekly%20Log%20%F0%9F%93%9D&first_page_size=50)
- 🚨 [My support Fieldnotes](https://gitlab.com/gitlab-com/support/fieldnotes/-/issues/?sort=created_date&state=opened&assignee_username%5B%5D=mlockhart) ([recording and collaborating](https://about.gitlab.com/handbook/support/workflows/fieldnote_issues.html) on customer problems)
- ✏️ [Contribution search](https://mlockhart.gitlab.io/user-contribution-search/)

## [![](https://gitlab.com/uploads/-/system/project/avatar/19846551/gl-notebook.png?width=64)](https://gitlab.com/mlockhart/lab) Lab Book

Latest logs and experiments from [my work lab](https://gitlab.com/mlockhart/lab).

<!---BEGIN-BLOG--->
|📙Personal log | Updated||🧪Experiment | Updated|
|--|--|--|--|--|
|[20241113:21 - Celebrating some small wins](https://gitlab.com/mlockhart/lab/-/issues/242) | 2024-11-13T21:26:54Z||[20241001:23 - Duo Chat: refactoring bash to POSIX, and back](https://gitlab.com/mlockhart/lab/-/issues/241) | 2024-10-03T21:50:03Z|
|[20240712:08 - PostgreSQL REINDEX for glibc update](https://gitlab.com/mlockhart/lab/-/issues/237) | 2025-02-28T06:44:21Z||[20240821:03 - Duo - sus2, or not sus2, that is the question](https://gitlab.com/mlockhart/lab/-/issues/240) | 2024-08-22T04:21:42Z|
|[20230701:00 - Work cycles](https://gitlab.com/mlockhart/lab/-/issues/197) | 2023-07-01T02:01:07Z||[20240731:03 - Teach me some chords, Duo](https://gitlab.com/mlockhart/lab/-/issues/239) | 2024-08-21T03:51:50Z|
|[20230321:08 - Playing with the Plex](https://gitlab.com/mlockhart/lab/-/issues/185) | 2023-03-30T21:02:28Z||[20240730:23 - GitLab Duo Chat and Rails](https://gitlab.com/mlockhart/lab/-/issues/238) | 2024-08-14T22:13:47Z|
||[📚See all logs](https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=log%3A%3Apersonal&first_page_size=50)|||[🔬See all experiments](https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=log%3A%3Aexperiment&first_page_size=50)||
<!---END-BLOG--->

## 🧪 Personal Groups

I have _many_ groups through work, so the [Groups tab](https://gitlab.com/users/mlockhart/groups) is less helpful. Here are my main top-level groups.

|  [![mlockhart-hax](https://gitlab.com/uploads/-/system/group/avatar/13452209/matrix-animated-image.gif?width=128)<br/>mlockhart-hax](https://gitlab.com/mlockhart-hax) <br/> Public work experiments  | [![mlockhart-fork](https://gitlab.com/uploads/-/system/group/avatar/9248097/fork.png?width=128) <br/> mlockhart-fork](https://gitlab.com/mlockhart-fork) <br/> A group to fork projects into | [![mlockhart_ultimate_group](https://gitlab.com/uploads/-/system/group/avatar/70922663/hacker-design.jpg?width=128)<br/>support - mlockhart_ultimate](https://gitlab.com/mlockhart_ultimate_group) <br/> For testing support issues<br/>with Ultimate features |
| -- | -- | -- |
|  [![mlockhart-private](https://gitlab.com/uploads/-/system/group/avatar/12086431/padlock.jpg?width=128) <br/> mlockhart-private](https://gitlab.com/mlockhart-private) <br/> Secret work projects |  [![mlockhart-attic](https://gitlab.com/uploads/-/system/group/avatar/12086447/Screen_Shot_2021-05-16_at_17.18.46.png?width=128)<br/> mlockhart-attic](https://gitlab.com/mlockhart-attic) <br/> Dusty old projects are here,<br/>where they may bitrot in peace  | [![mlockhart_premium_group](https://gitlab.com/uploads/-/system/group/avatar/70922664/glider.jpg?width=128)<br/>support - mlockhart_premium](https://gitlab.com/mlockhart_premium_group) <br/> For testing support issues<br/>with Premium features   |

