require 'rss'
require 'time'
require 'optparse'

# URLs

# Note: Requires the RSS Feed Token be in an environment variable called FEED_TOKEN
PERSONAL_LOG = "https://gitlab.com/mlockhart/lab/-/issues.atom?feed_token=#{ENV["FEED_TOKEN"]}&sort=created_date&state=all&label_name[]=log%3A%3Apersonal"
ALL_LOGS_URL = 'https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=log%3A%3Apersonal&first_page_size=50'

EXPERIMENTS_LOG =  "https://gitlab.com/mlockhart/lab/-/issues.atom?feed_token=#{ENV["FEED_TOKEN"]}&state=all&label_name[]=log%3A%3Aexperiment"
ALL_EXPERIMENTS_URL = 'https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=log%3A%3Aexperiment&first_page_size=50'

# README blogs markers
BEGIN_BLOG = "<!---BEGIN-BLOG--->\n"
END_BLOG = "<!---END-BLOG--->"
BLOGS_REGEX = /<!---BEGIN-BLOG--->(.*)<!---END-BLOG--->/im
README = "README.md"

# Get the blogs
def remove_xml_tags(thing)
  re = /<("[^"]*"|'[^']*'|[^'">])*>/
  return thing.gsub!(re, '')
end

def list_issues(url,max)
  feeds_list = []
  count = 0

  URI.open(url) do |rss|
    feed = RSS::Parser.parse(rss, do_validate=false, ignore_unknown_element=true)
    feed.items.each do |item|
      title = remove_xml_tags item.title.to_s
      link = item.link.to_s.gsub('<link href="','').gsub('"/>','')
      updated = remove_xml_tags item.updated.to_s
      blog_item = "|[#{title}](#{link}) | #{updated}|"
      feeds_list.append blog_item
      count += 1
      break unless count < max
    end
  end
  return feeds_list
end

def list_blogs(max_items = 8)
  logs = list_issues PERSONAL_LOG,max_items
  experiments = list_issues EXPERIMENTS_LOG,max_items

  feeds_list = BEGIN_BLOG
  feeds_list.concat "|📙Personal log | Updated||🧪Experiment | Updated|\n"
  feeds_list.concat "|--|--|--|--|--|\n"
  logs.select.with_index do |log, idx|
    feeds_list.concat "#{log}#{experiments[idx]}\n"
  end
  feeds_list.concat "||[📚See all logs](#{ALL_LOGS_URL})|"
  feeds_list.concat "||[🔬See all experiments](#{ALL_EXPERIMENTS_URL})||\n"
  feeds_list.concat END_BLOG
  puts feeds_list
  return feeds_list
end

@options = {
  entries: 6
}
OptionParser.new do |opt|
  opt.banner = "Usage: #{__FILE__} [options]"
  opt.on('e', '--entries COUNT', Integer, "Number of entries to fetch") do |o|
     @options[:entries] = o
  end
end.parse!

# Load the README
readme_file = File.open README
readme_buffer = readme_file.read
readme_file.close

# Insert the blogs
File.write README, readme_buffer.gsub(BLOGS_REGEX, list_blogs(@options[:entries]))
